/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class MisuseWithException {

/* Setup */

	/**
	 * Initialize Current object.
	 */
	constructor ()
	{
		s = ""
	}

	companion object {
		/**
		 * A completely initialized object.
		 */
		val a: MisuseWithException = MisuseWithException ()
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	var s: String

	/**
	 * A reference to the other object (if any).
	 */
	var item: MisuseWithException? = null

	/**
	 * Register the new object in the context object `a` and throw an exception before the initializaion completes.
	 */
	constructor (@Suppress("UNUSED_PARAMETER") unused: Unit)
	{
		a.item = this
		raise ()
		s = ""
	}
    
        /**
         * Raise an exception.
         */
	fun raise ()
	{
		throw Exception ()
	}

}

/* Setup */

/**
 * Create a new object and catch an exception to retrieve it in an uninitialized state.
 */
fun main (args: Array <String>)
{
	try {
		MisuseWithException (Unit)
	}
	catch (e: Exception) {
		val x = MisuseWithException.a.item
		if (x != null) {
			println (x.s.count ())
		}
	}
}
