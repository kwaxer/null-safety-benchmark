/**
 * Copyright (C) 2017-2020 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class MisuseWithThread {

/* Setup */

	/**
	 * Create a fully initialized object and pass it to the one to be created.
	 */
	public static void main (String [] args) throws InterruptedException {
		MisuseWithThread a = new MisuseWithThread ();
			/* Launch a new thread. */
		new Thread (new Runnable () {
			public void run () {
				MisuseWithThread b = null;
				while (b == null) {
					try {
						Thread.sleep (1);
					}
					catch (Exception e) {};
					b = a.item;
				}
				MisuseWithThread x = b.value.value;
			}
		}).start ();
			/* Perform the test. */
		new MisuseWithThread (a);
	}

	/**
	 * Initialize Current object.
	 */
	MisuseWithThread ()
	{
		value = this;
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	MisuseWithThread value;

	/**
	 * A reference to the other object (if any).
	 */
	@javax.annotation.Nullable
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	MisuseWithThread item;

	/**
	 * Register a new object in the object `referer` and initialize itself afterwards.
	 */
	MisuseWithThread (MisuseWithThread referer) throws InterruptedException
	{
		referer.item = this;
		Thread.sleep (1_000);
		value = this;
	}
    
}
