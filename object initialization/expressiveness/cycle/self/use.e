note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class USE

create
	make,
	test

feature {NONE} -- Testing

	make
			-- Create an object referencing itself.
		local
			a: USE
		do
				-- Initialize Current object.
			item := Current
				-- Perform the test.
			create a.test
			io.put_string (if a = a.item then "OK" else "FAILED" end)
			io.put_new_line
		end

feature -- Testing

	item: USE
			-- A reference to the other object.

	test
			-- Initialize a new object with a reference to itself.
		do
			item := Current
		end

end
