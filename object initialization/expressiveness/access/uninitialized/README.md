# Using an incompletely initialized object #

## Scenario ##

1. Create an incompletely initialized object A.
2. Pass A as an argument to the constructor of an object B.
3. Make a call on the object A from the constructor of the object B.
4. Initialize B in its constructor as required.
5. Complete initialization of the object A.

## Expected results ##

### Sound result ###

No error.

### Expectations from theory and documentation ###

Framework              | Compile time | Run-time output | Expressive?
-----------------------|:------------:|:---------------:|:-----------:
Eiffel                 | Error        |                 | No
Java Checker Framework | No error     | `OK`            | Yes
Kotlin                 | No error     | `OK`            | Yes
