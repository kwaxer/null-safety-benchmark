/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class Use {

/* Testing */

	/**
	 * A value of the object.
	 */
	var item: String

	/**
	 * Some data.
	 */
	var data: String

	/**
	 * Pass Current object to a new one for making a callback and complete initialization afterwards.
	 */
	constructor ()
	{
		item = "FAILED"
		Use (this)
		data = "FAILED"
	}

	/**
	 * Initialize a new object and make a callback on `referer`.
	 */
	constructor (referer: Use)
	{
		item = "FAILED"
		data = "FAILED"
		if ((referer.item != null) && (referer.data == null)) {
			referer.put ("OK")
		}
	}

	/**
	 * Set `item` to `value`.
	 */
	fun put (value: String)
	{
		item = value
	}
}

/**
 * Create an incompletely initialized object and pass it to the one to be created that makes a callback to the first object.
 */
fun main(args: Array <String>) {
	println ((Use ()).item)
}
