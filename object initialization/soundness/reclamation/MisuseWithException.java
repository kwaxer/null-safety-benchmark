/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class MisuseWithException {

/* Testing */

	/**
	 * Raise an exception instead of initializing a new object.
	 */
	MisuseWithException () throws Exception
	{
		throw new Exception ();
	}

	/*
	 * A value that should be set before the object is used.
	 */
	String s;

	/**
	 * Use `s`.
	 */
	protected void finalize () {
		try {
				/* Print length of `s`. */
			System.out.println (s.length ());
		}
		catch (NullPointerException e) {
				/* Report NullPointerException. */
			System.out.println ("NPE");
		}
	}

/* Setup */

	/**
	 * Create a new object without keeping a reference to it and trigger garbage collection.
	 */
	public static void main (String [] args) throws InterruptedException {
		int i = 1000;
		while (i-- > 0) {
				/* Try to create a new object. */
			try {
				new MisuseWithException ();
			}
			catch (Exception e) {
			}
				/* Request GC to run finalizers. */
			System.gc ();
		}
			/* Wait for GC to finish. */
		Thread.sleep (1000);
	}
}
