/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class MisuseWithException {

/* Setup */

	/**
	 * Create a new object and catch an exception to retrieve it in an uninitialized state.
	 */
	public static void main (String [] args) {
		try {
			new MisuseWithException (0);
		}
		catch (Exception e) {
			MisuseWithException b = a.item;
			if (b != null) {
				MisuseWithException x = b.value.value;
			}
		}
	}

	/**
	 * Initialize Current object.
	 */
	MisuseWithException ()
	{
		value = this;
	}

	/**
	 * A completely initialized object.
	 */
	static MisuseWithException a = new MisuseWithException ();

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	MisuseWithException value;

	/**
	 * A reference to the other object (if any).
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	@javax.annotation.Nullable
	MisuseWithException item;

	/**
	 * Register the new object in the context object `a` and throw an exception before the initializaion completes.
	 */
	MisuseWithException (int unused) throws Exception
	{
		a.item = this;
		throw new Exception ();
	}
    
}
