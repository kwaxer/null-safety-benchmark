note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MISUSE_WITH_THREAD

inherit

	THREAD
		rename
			make as thread_make
		end

create
	make,
	make_empty,
	test

feature {NONE} -- Setup

	make
			-- Create a fully initialized object and pass it to the one to be created.
		local
			a: MISUSE_WITH_THREAD
		do
				-- Initialize Current object.
			s := ""
			create a.make_empty
			item := a
			thread_make
				-- Launch a new thread.
			launch
				-- Perform the test.
			;(create {MISUSE_WITH_THREAD}.test (a)).do_nothing
		end

	make_empty
			-- Initialize Current object.
		do
			s := ""
			thread_make
		end

	execute
		do
			if attached item as a then
				from
				until
					attached a.item as b
				loop
					sleep (1_000_000)
				end
				io.put_integer (b.s.count)
				io.put_new_line
			end
		end

feature -- Testing

	s: STRING
			-- A value that is set before the object is used.

	item: detachable MISUSE_WITH_THREAD
			-- A reference to the other object (if any).

	put (value: like item)
			-- Set `item` to `value`.
		do
			item := value
		end

	test (referer: MISUSE_WITH_THREAD)
			-- Register a new object in `referer` and initialize it afterwards.
		do
			thread_make
			referer.put (Current)
			sleep (1_000_000_000)
			s := ""
		end

end
