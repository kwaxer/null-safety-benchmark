@echo off

if not defined CHECKER_FRAMEWORK set CHECKER_FRAMEWORK=%~dp0checker-framework-3.9.0
if not defined JSR305_JAR set JSR305_JAR=%~dp0jsr305-3.0.2.jar

java -jar "%CHECKER_FRAMEWORK%\checker\dist\checker.jar" -classpath "%JSR305_JAR%" -processor nullness -AconcurrentSemantics %*
