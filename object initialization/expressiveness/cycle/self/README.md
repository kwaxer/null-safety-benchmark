# Creation of an object referencing itself #

## Scenario ##

1. Create an object A.
2. Initialize the object A to reference the object A in the constructor.

## Expected results ##

### Sound result ###

No error.

### Expectations from theory and documentation ###

Framework              | Compile time | Run-time output | Expressive?
-----------------------|:------------:|:---------------:|:-----------------:
Eiffel                 | No error     | `OK`            | Yes
Java Checker Framework | No error     | `OK`            | Yes
Kotlin                 | No error     | `OK`            | Yes
