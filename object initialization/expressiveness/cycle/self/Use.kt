/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class Use {

/* Testing */

	/**
	 * A reference to the other object.
	 */
	var item: Use

	/**
	 * Initialize a new object with a reference to itself.
	 */
	constructor ()
	{
		item = this
	}

}

/**
 * Create an object referencing itself.
 */
fun main(args: Array <String>) {
	var a: Use = Use ()
	println (if (a == a.item) "OK" else "FAILED");
}
