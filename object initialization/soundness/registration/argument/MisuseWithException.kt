/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class MisuseWithException {

/* Setup */

	/**
	 * Initialize Current object.
	 */
	constructor ()
	{
		s = ""
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	var s: String

	/**
	 * A reference to the other object (if any).
	 */
	var item: MisuseWithException? = null

	/**
	 * Initialize a new object and register it in `referer`.
	 */
	constructor (referer: MisuseWithException)
	{
		referer.item = this
		raise ()
		s = ""
	}
    
        /**
         * Raise an exception.
         */
	fun raise ()
	{
		throw Exception ()
	}

}

/* Setup */

/**
 * Create a new object and catch an exception to retrieve it in an uninitialized state.
 */
fun main(args: Array<String>)
{
	var a: MisuseWithException = MisuseWithException ();
	try {
		MisuseWithException (a)
	}
	catch (e: Exception) {
		val x = a.item
		if (x != null) {
			println (x.s.count ())
		}
	}
}
