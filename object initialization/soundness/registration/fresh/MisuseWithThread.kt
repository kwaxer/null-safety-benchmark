/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class MisuseWithThread {

/* Setup */

	/**
	 * Initialize the current object and pass it to create a new one that records itself in the current object.
	 * Access the other object from a different thread before it finishes its initialization.
	 */
	constructor ()
	{
			/* Initialize Current object. */
		s = ""
			/* Launch a new thread. */
		kotlin.concurrent.thread {
			var x: MisuseWithThread? = null
				/* Wait until `item` is set. */
			while (x == null) {
				Thread.sleep (1)
				x = item
			}
				/* Access the object referenced by `item`. */
			println (x.s.count ())
		}
			/* Perform the test. */
		MisuseWithThread (this)
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	var s: String

	/**
	 * A reference to the other object (if any).
	 */
	var item: MisuseWithThread? = null

	/**
	 * Register a new object in `referer` and initialize it afterwards.
	 */
	constructor (referer: MisuseWithThread)
	{
		referer.item = this
		Thread.sleep (1_000)
		s = ""
	}
    
}

/**
 * Create a new object that will initialize and pass itself to another new one.
 */
fun main (args: Array <String>) {
	MisuseWithThread ();
}
