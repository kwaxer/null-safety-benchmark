/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class MisuseWithException {

/* Setup */

	/**
	 * Create a new object that will initialize and pass itself to another new one.
	 */
	public static void main (String [] args) {
		new MisuseWithException ();
	}

	/**
	 * Initialize the current object and pass it to create a new one that raises an exception before finishing its initialization.
         * Catch the exception and access the incompletely initialized object from the current one.
	 */
	MisuseWithException ()
	{
			/* Initialize Current object. */
		s = "";
		item = this;
			/* Perform the test. */
		try {
			new MisuseWithException (this);
		}
		catch (Exception e) {
			String x = item.item.s;
			if (x != null) {
				System.out.println (x.length ());
			}
		}
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	String s;

	/**
	 * A reference to the other object (if any).
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	MisuseWithException item;

	/**
	 * Register the new object in `referer` and throw an exception before initializaion completes.
	 */
	MisuseWithException (@org.checkerframework.checker.initialization.qual.UnderInitialization MisuseWithException referer) throws Exception
	{
		referer.item = this;
		throw new Exception ();
	}
    
}
