/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class MisuseWithException {

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	var s: String

	/**
	 * Raise an exception before initializing the new object.
	 */
	constructor ()
	{
		raise ()
		s = ""
	}

    	/**
    	 * Raise an exception with a reference to the current object.
    	 */
	fun raise ()
	{
		throw MyException (this)
	}

}

class MyException (val value: MisuseWithException) : Exception () {
}

/**
 * Create a new object and catch an exception to retrieve it in an uninitialized state.
 */
fun main(args: Array<String>) {
	try {
		MisuseWithException ()
	}
	catch (e: MyException) {
		println (e.value.s.count ())
	}
}
