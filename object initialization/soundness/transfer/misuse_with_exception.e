note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MISUSE_WITH_EXCEPTION

create
	make,
	test

feature {NONE} -- Setup

	make
			-- Create a new object without keeping a reference to it and trigger garbage collection.
		local
			is_retried: BOOLEAN
		do
				-- Initialize Current object.
			s := ""
			if not is_retried then
					-- Perform the test.
				;(create {MISUSE_WITH_EXCEPTION}.test).do_nothing
			end
		rescue
			if attached {MY_EXCEPTION} (create {EXCEPTION_MANAGER}).last_exception as e then
				io.put_integer (e.value.s.count)
			end
			is_retried := True
			retry
		end

feature -- Testing

	s: STRING
			-- A value that should be set before the object is used.

	test
	    		-- Raise an exception with a reference to the current object before initializing it.
		do
			;(create {MY_EXCEPTION}.make (Current)).raise
			s := ""
		end

end
