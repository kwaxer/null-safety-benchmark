note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MISUSE_WITH_EXCEPTION

create
	make,
	test

feature {NONE} -- Setup

	make
			-- Initialize the current object and pass it to create a new one that raises an exception before finishing its initialization.
			-- Catch the exception and access the incompletely initialized object from the current one.
		local
			is_retried: BOOLEAN
		do
				-- Initialize Current object.
			s := ""
			if not is_retried then
					-- Perform the test.
				;(create {MISUSE_WITH_EXCEPTION}.test (Current)).do_nothing
			end
		rescue
			if attached item as x then
				io.put_integer (x.s.count)
				io.put_new_line
			end
			is_retried := True
			retry
		end

feature -- Testing

	s: STRING
			-- A value that is set before the object is used.

	item: detachable MISUSE_WITH_EXCEPTION
			-- A reference to the other object (if any).

	put (value: like item)
			-- Set `item` to `value`.
		do
			item := value
		end

	test (referer: MISUSE_WITH_EXCEPTION)
			-- Register a new object in `referer` and initialize it afterwards.
		do
				-- Register the current incompletely initialized object in `referer`.
			referer.put (Current)
				-- Raise an exception.
			check False then end
				-- Complete the initialization.
			s := ""
		end

end
