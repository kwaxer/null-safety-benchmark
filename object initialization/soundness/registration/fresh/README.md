# Registration of a new object in another new completely initialized object passed as an argument to the constructor #

## Problematic scenario ##

1. Create and fully initialize an object A.
2. Inside the constructor of A, pass it as an argument to the constructor of an object B.
3. Register B in the object A so that the object A refers to the object B.
4. Access B before it is completely initialized.

## Examples ##

The examples are distinguished by how the step 4 is achieved. The regular execution is interrupted with

* an exception
* concurrent execution in another thread

## Expected results ##

### Sound result ###

Compile-time error.

### Expectations from theory and documentation ###

Framework              | Compile time | Sound?
-----------------------|:------------:|:------------:
Eiffel                 | Error        | Yes
Java Checker Framework | Error        | Yes
Kotlin                 | No error     | No
