/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class MisuseWithException {

/* Setup */

	/**
	 * Initialize the current object and pass it to create a new one that raises an exception before finishing its initialization.
         * Catch the exception and access the incompletely initialized object from the current one.
	 */
	constructor ()
	{
			/* Initialize Current object. */
		s = ""
			/* Perform the test. */
		try {
			MisuseWithException (this)
		}
		catch (e: Exception) {
			val x = item
			if (x != null) {
				println (x.s.count ())
			}
		}
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	var s: String

	/**
	 * A reference to the other object (if any).
	 */
	var item: MisuseWithException? = null

	/**
	 * Register the new object in `referer` and throw an exception before initializaion completes.
	 */
	constructor (referer: MisuseWithException)
	{
		referer.item = this
		raise ()
		s = ""
	}
    
        /**
         * Raise an exception.
         */
	fun raise ()
	{
		throw Exception ()
	}

}

/**
 * Create a new object that will initialize and pass itself to another new one.
 */
fun main (args: Array <String>) {
	MisuseWithException ();
}
