# Null safety benchmark #

This repository contains benchmark examples to test and compare different null safety solutions for their soundness and expressiveness.

[]: # (### What is this repository for? ###)

## Repository structure ##

* object initialization – examples of initialization of objects with non-null fields
	* soundness – examples of the code that leads to null pointer exceptions and should not be permitted by sound rules
		* registration – registration of a new object in another one
			* [argument](/object%20initialization/soundness/registration/argument/) – the existing object is passed to the new one an as an argument to the constructor
			* [fresh](/object%20initialization/soundness/registration/fresh/) – the newly created completely initialized object is passed from its constructor as an argument to another constructor
			* [context](/object%20initialization/soundness/registration/context/) – the existing object is retrieved in the new one from the execution context
		* [reclamation](/object%20initialization/soundness/reclamation/) – access to a non-null object fields from a finalizer
		* [transfer](/object%20initialization/soundness/transfer/) – out-of-order object transfer from the initialization code to the code where only completely initialized objects are expected
	* expressiveness – examples of the code that does not lead to null pointer exceptions and may be permitted depending on how strict the rules are
		* access – access to a newly created object from its constructor
			* initialized – the object is completely initialized
				* [callback](/object%20initialization/expressiveness/access/initialized/callback) – a completely initialized object passes itself from its constructor to another constructor
				* registration – registration of a new object in an existing one
					* [argument](/object%20initialization/expressiveness/access/initialized/registration/argument) – the existing object is passed as an argument to the constructor
					* [context](/object%20initialization/expressiveness/access/initialized/registration/context) – the existing object is retrieved from the execution context
			* [uninitialized](/object%20initialization/expressiveness/access/uninitialized) – the object is incompletely initialized
		* cycle – structures with circular references
			* [self](/object%20initialization/expressiveness/cycle/self) – a new object references itself after creation
			* [mutual](/object%20initialization/expressiveness/cycle/mutual) – two or more objects reference each other after creation

[]: # (### How do I get set up? ###)

[]: # (* Summary of set up)
[]: # (* Configuration)
[]: # (* Dependencies)
[]: # (* Database configuration)
[]: # (* How to run tests)
[]: # (* Deployment instructions)

[]: # (### Contribution guidelines ###)

[]: # (* Writing tests)
[]: # (* Code review)
[]: # (* Other guidelines)

[]: # (### Who do I talk to? ###)

[]: # (* Repo owner or admin)
[]: # (* Other community or team contact)