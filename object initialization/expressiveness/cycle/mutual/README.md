# Creation of new objects mutually referencing each other #

## Scenario ##

1. Create an object A.
2. Pass A as an argument to the constructor of an object B.
3. Pass A as an argument to the constructor of an object C.
4. Initialize C in its constructor as required, reference A from C and exit the constructor.
5. Initialize B in its constructor as required, reference C from B and exit the constructor.
6. Initialize A in its constructor as required, reference B from A and exit the constructor.

## Expected results ##

### Sound result ###

No error.

### Expectations from theory and documentation ###

Framework              | Compile time | Run-time output | Expressive?
-----------------------|:------------:|:---------------:|:-----------------:
Eiffel                 | No error     | `OK`            | Yes
Java Checker Framework | No error     | `OK`            | Yes
Kotlin                 | No error     | `OK`            | Yes
