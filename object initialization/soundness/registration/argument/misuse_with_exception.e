note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MISUSE_WITH_EXCEPTION

create
	make,
	make_empty,
	test

feature {NONE} -- Setup

	make
			-- Create a fully initialized object and pass it to the one to be created.
		local
			a: MISUSE_WITH_EXCEPTION
			is_retried: BOOLEAN
		do
				-- Initialize Current object.
			s := ""
			if not is_retried then
					-- Perform the test.
				create a.make_empty
				;(create {MISUSE_WITH_EXCEPTION}.test (a)).do_nothing
			end
		rescue
			if attached a and then attached a.item as b then
				io.put_integer (b.s.count)
				io.put_new_line
			end
			is_retried := True
			retry
		end

	make_empty
			-- Initialize Current object.
		do
			s := ""
		end

feature -- Testing

	s: STRING
			-- A value that is set before the object is used.

	item: detachable MISUSE_WITH_EXCEPTION
			-- A reference to the other object (if any).

	put (value: like item)
			-- Set `item` to `value`.
		do
			item := value
		end

	test (referer: MISUSE_WITH_EXCEPTION)
			-- Register a new object in `referer` and initialize it afterwards.
		do
				-- Register the current incompletely initialized object in `referer`.
			referer.put (Current)
				-- Raise an exception.
			check False then end
				-- Complete the initialization.
			s := ""
		end

end
