note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MY_EXCEPTION

inherit
	EXCEPTION
		redefine
			code
		end

create
	make

feature {NONE} -- Creation

	make (v: MISUSE_WITH_EXCEPTION)
			-- Create an object referenceing `v`.
		do
			value := v
		ensure
			value_set: value = v
		end

feature -- Access

	code: like {EXCEPTION}.code
		do
			Result := {EXCEP_CONST}.developer_exception
		end

	value: MISUSE_WITH_EXCEPTION
			-- An associated object.

end
