/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class MisuseWithThread {

/* Setup */

	/**
	 * Initialize Current object.
	 */
	constructor ()
	{
		s = ""
	}

	companion object {
		/**
		 * A completely initialized object.
		 */
		val a: MisuseWithThread = MisuseWithThread ()
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	var s: String

	/**
	 * A reference to the other object (if any).
	 */
	var item: MisuseWithThread? = null

	/**
	 * Register a new object in the context object `a` and initialize itself afterwards.
	 */
	constructor (@Suppress("UNUSED_PARAMETER") unused: Unit)
	{
		a.item = this
		Thread.sleep (1_000)
		s = ""
	}
    
}

/**
 * Create a fully initialized object and pass it to the one to be created.
 */
fun main(args: Array<String>) {
		/* Launch a new thread. */
	kotlin.concurrent.thread {
		var b: MisuseWithThread? = null
		while (b == null) {
			Thread.sleep (1)
			b = MisuseWithThread.a.item
		}
		println (b.s.count ())
	}
		/* Perform the test. */
	MisuseWithThread (Unit)
}