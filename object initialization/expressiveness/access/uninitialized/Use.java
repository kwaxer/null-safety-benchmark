/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class Use {

/* Setup */

	/**
	 * Create an incompletely initialized object and pass it to the one to be created that makes a callback to the first object.
	 */
	public static void main (String [] args)
	{
			/* Perform the test. */
		System.out.println ((new Use ()).item);
	}

/* Testing */

	/**
	 * A value of the object.
	 */
	String item;

	/**
	 * Some data.
	 */
	String data;

	/**
	 * Pass Current object to a new one for making a callback and complete initialization afterwards.
	 */
	Use ()
	{
		item = "FAILED";
		new Use (this);
		data = "FAILED";
	}

	/**
	 * Initialize a new object and make a callback on `referer`.
	 */
	Use (@org.checkerframework.checker.initialization.qual.UnderInitialization Use referer)
	{
		item = "FAILED";
		data = "FAILED";
		if ((referer.item != null) && (referer.data == null)) {
			referer.put ("OK");
		}
	}

	/**
	 * Set `item` to `value`.
	 */
	void put (@org.checkerframework.checker.initialization.qual.UnderInitialization Use this, String value)
	{
		item = value;
	}

}