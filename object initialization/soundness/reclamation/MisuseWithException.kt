/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class MisuseWithException {

	/**
	 * Initialize a new object.
	 */
	constructor ()
	{
		raise ()
		s = ""
	}

		/* A value that should be set before the object is used. */
	var s: String

	/**
	 * Raise an exception.
	 */
	fun raise ()
	{
		throw Exception ()
	}

	/**
	 * Use `s`.
	 */
	protected fun finalize() {
		try {
				/* Print length of `s`. */
			println (s.count ())
		}
		catch (e: NullPointerException) {
				/* Report NullPointerException. */
			println ("NPE")
		}
	}

}

/**
 * Create a new object without keeping a reference to it and trigger garbage collection.
 */
fun main (args: Array<String>) {
	var i = 1_000
	while (i-- > 0) {
			/* Try to create a new object. */
		try {
			MisuseWithException ();
		}
		catch (e: Exception) {
		}
			/* Request GC to run finalizers. */
		System.gc ()
	}
		/* Wait for GC to finish. */
	Thread.sleep (1_000)
}