note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class USE

create
	make,
	test,
	test_1,
	test_2

feature {NONE} -- Setup

	make
			-- Create a cycle of 3 objects.
		local
			a, b: USE
			i: INTEGER
		do
				-- Initialize Current object.
			item := Current
				-- Perform the test.
			create a.test
			from
				b := a.item
			until
				b = a
			loop
				i := i + 1
				b := b.item
			end
			io.put_string (if i = 2 then "OK" else "FAILED" end)
			io.put_new_line
		end

feature -- Testing

	item: USE
			-- A reference to the other object.

	test
			-- Initialize a new object using other new objects linked in a cycle of 3 objects.
		do
			create item.test_2 (Current)
		end

	test_2 (root: USE)
			-- Initialize a new object by creating one new object referencing `root`.
		do
			create item.test_1 (root)
		end

	test_1 (root: USE)
			-- Initialize a new object referencing `root`.
		do
			item := root
		end

end
