/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class Use {

/* Setup */

	/**
	 * Create a fully initialized object and pass it to the one to be created.
	 */
	public static void main (String [] args)
	{
			/* Perform the test. */
		Use a = new Use ();
		new Use (a);
		Use b = a.item;
		if (b != null) {
			System.out.println (b.s);
		}
		else {
			System.out.println ("FAILED");
		}
	}

	/**
	 * Initialize Current object.
	 */
	Use ()
	{
		s = "";
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	String s;

	/**
	 * A reference to the other object (if any).
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	@javax.annotation.Nullable
	Use item;

	/**
	 * Initialize a new object and register it in `referer`.
	 */
	Use (Use referer)
	{
		s = "OK";
		referer.item = this;
	}

}