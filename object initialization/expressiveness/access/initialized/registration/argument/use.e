note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class USE

create
	make,
	make_empty,
	test

feature {NONE} -- Setup

	make
			-- Create a fully initialized object and pass it to the one to be created.
		local
			a: USE
		do
				-- Initialize Current object.
			s := ""
				-- Perform the test.
			create a.make_empty
			;(create {USE}.test (a)).do_nothing
			if attached a.item as b then
				io.put_string (b.s)
			else
				io.put_string ("FAILED")
			end
		end

	make_empty
			-- Initialize Current object.
		do
			s := ""
		end

feature -- Testing

	s: STRING
			-- A value that is set before the object is used.

	item: detachable USE
			-- A reference to the other object (if any).

	put (value: like item)
			-- Set `item` to `value`.
		do
			item := value
		end

	test (referer: USE)
			-- Initialize a new object and register it in `referer`.
		do
			s := "OK"
			referer.put (Current)
		end

end