note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MISUSE_WITH_EXCEPTION

create
	make,
	make_empty,
	test

feature {NONE} -- Setup

	make
			-- Create a fully initialized object then create another one that will use the first.
		local
			is_retried: BOOLEAN
		do
				-- Initialize Current object.
			s := ""
			if not is_retried then
					-- Perform the test.
				;(create {MISUSE_WITH_EXCEPTION}.test).do_nothing
			end
		rescue
			if attached a.item as b then
				io.put_integer (b.s.count)
				io.put_new_line
			end
			is_retried := True
			retry
		end

	make_empty
			-- Initialize Current object.
		do
			s := ""
		end

	a: MISUSE_WITH_EXCEPTION
			-- A completely initialized object.
		once
			create Result.make_empty
		end

feature -- Testing

	s: STRING
			-- A value that is set before the object is used.

	item: detachable MISUSE_WITH_EXCEPTION
			-- A reference to the other object (if any).

	put (value: like item)
			-- Set `item` to `value`.
		do
			item := value
		end

	test
			-- Register a new object in the object `a` and initialize itself afterwards.
		do
				-- Register the current incompletely initialized object in the existing one.
			a.put (Current)
				-- Raise an exception.
			check False then end
				-- Complete the initialization.
			s := ""
		end

end
