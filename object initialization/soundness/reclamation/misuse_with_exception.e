note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MISUSE_WITH_EXCEPTION

inherit
	MEMORY
		redefine
			dispose
		end

create
	make,
	test

feature {NONE} -- Setup

	make
			-- Create a new object without keeping a reference to it and trigger garbage collection.
		local
			is_retried: BOOLEAN
		do
				-- Initialize Current object.
			s := ""
			if not is_retried then
					-- Start test.
				;(create {MISUSE_WITH_EXCEPTION}.test).do_nothing
			end
		rescue
				-- An object of type {MISUSE_WITH_EXCEPTION} has been created.
				-- Trigger GC to call `dispose`.
			full_collect
			is_retried := True
			retry
		end

feature -- Testing

	s: STRING
			-- A value that should be set before the object is used.

	test
			-- Raise an exception instead of initializing a new object.
		do
			check False then end
			s := ""
		end

	dispose
			-- Access `s`.
		do
			s.do_nothing
		end

end
