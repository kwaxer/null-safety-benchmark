/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class Use {

/* Setup */

	/**
	 * Create a cycle of 3 objects.
	 */
	public static void main (String [] args)
	{
			/* Perform the test. */
		Use a = new Use ();
		Use b = a.item;
		int i = 0;
		while (b != a) {
			i = i + 1;
			b = b.item;
		}
		System.out.println (i == 2? "OK": "FAILED");
	}

/* Testing */

	/**
	 * A reference to the other object.
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	Use item;

	/**
	 * Initialize a new object using other new objects linked in a cycle of 3 objects.
	 */
	Use ()
	{
		item = new Use (this, 2);
	}

	/**
	 * Initialize a new object using other new objects linked in a chain `count` objects terminating with `root`.
	 */
	Use (@org.checkerframework.checker.initialization.qual.UnderInitialization Use root, int count)
	{
		if (count > 1) {
			item = new Use (root, count - 1);
		}
		else {
			item = root;
		}
	}

}
