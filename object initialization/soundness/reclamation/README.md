# Reclamation of an incompletely initialized object #

## Problematic scenario ##

1. Start a creation of a new object by calling its constructor.
2. Raise an exception in the constructor before the initialization is finished.
3. Trigger garbage collection to make sure the object created at step 1 is reclaimed.
4. Access an uninitialized field of the object.

## Expected results ##

### Sound result ###

Compile-time error.

### Expectations from theory and documentation ###

Framework              | Compile time | Sound?
-----------------------|:------------:|:------------:
Eiffel                 | No error     | No
Java Checker Framework | No error     | No
Kotlin                 | No error     | No
