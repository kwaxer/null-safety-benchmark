@echo off

if not defined CHECKER_FRAMEWORK set CHECKER_FRAMEWORK=%~dp0checker-framework-2.2.1
if not defined JSR305_JAR set JSR305_JAR=%~dp0jsr305-3.0.2.jar

call "%CHECKER_FRAMEWORK%\checker\bin\javac.bat" -classpath "%JSR305_JAR%" %*
