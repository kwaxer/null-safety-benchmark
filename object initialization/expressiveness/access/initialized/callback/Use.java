/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class Use {

/* Setup */

	/**
	 * Create a fully initialized object and pass it to the one to be created that makes a callback to the first object.
	 */
	public static void main (String [] args)
	{
			/* Perform the test. */
		System.out.println ((new Use ()).item);
	}

/* Testing */

	/**
	 * A value of the object.
	 */
	String item;

	/**
	 * Initialize Current object and pass it to a new one for making a callback.
	 */
	Use ()
	{
		item = "O";
		new Use (this);
	}

	/**
	 * Initialize a new object and make a callback on `referer`.
	 */
	Use (@org.checkerframework.checker.initialization.qual.UnderInitialization Use referer)
	{
		referer.append ("K");
		item = "FAILED";
	}

	/**
	 * Append `value` to `item`.
	 */
	void append (@org.checkerframework.checker.initialization.qual.UnderInitialization Use this, String value)
	{
		item = item.concat (value);
	}

}