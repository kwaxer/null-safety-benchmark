note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MISUSE_WITH_THREAD

inherit

	THREAD
		rename
			make as thread_make
		end

create
	make,
	make_empty,
	test

feature {NONE} -- Setup

	make
			-- Create a fully initialized object then create another one that will use the first.
		do
				-- Initialize Current object.
			s := ""
			thread_make
				-- Launch a new thread.
			launch
				-- Perform the test.
			;(create {MISUSE_WITH_THREAD}.test).do_nothing
		end

	make_empty
			-- Initialize Current object.
		do
			s := ""
			thread_make
		end

	execute
			-- <Precursor>
		do
				-- Wait until `a.item` is set.
			from
			until
				attached a.item as b
			loop
				sleep (1_000_000)
			end
				-- Access the object referenced by `item`.
			io.put_integer (b.s.count)
			io.put_new_line
		end

	a: MISUSE_WITH_THREAD
			-- A completely initialized object.
		once ("PROCESS")
			create Result.make_empty
		end

feature -- Testing

	s: STRING
			-- A value that is set before the object is used.

	item: detachable MISUSE_WITH_THREAD
			-- A reference to the other object (if any).

	put (value: like item)
			-- Set `item` to `value`.
		do
			item := value
		end

	test
			-- Register a new object in the object `a` and initialize itself afterwards.
		do
				-- Initialize data of {THREAD}.
			thread_make
				-- Register the current incompletely initialized object in the context object.
			a.put (Current)
				-- Let other thread access the current object.
			sleep (1_000_000_000)
				-- Complete initialization.
			s := ""
		end

end
