/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class MisuseWithThread {

/* Setup */

	/**
	 * Create a new object that will initialize and pass itself to another new one.
	 */
	public static void main (String [] args) throws InterruptedException {
		new MisuseWithThread ();
	}

	/**
	 * Initialize the current object and pass it to create a new one that records itself in the current object.
	 * Access the other object from a different thread before it finishes its initialization.
	 */
	MisuseWithThread () throws InterruptedException
	{
			/* Initialize Current object. */
		s = "";
		item = this;
			/* Launch a new thread. */
		new Thread (new Runnable () {
			public void run () {
				MisuseWithThread x = item.item;
					/* Wait until `item` is set. */
				while (x == item) {
					try {
						Thread.sleep (1);
					}
					catch (Exception e) {};
					x = item.item;
				}
					/* Access the object referenced by `item`. */
				String y = x.s;
				if (y != null) {
					System.out.println (y.length ());
				}
			}
		}).start ();
			/* Perform the test. */
		new MisuseWithThread (this);
	}

/* Testing */

	/**
	 * A value that should be set before the object is used.
	 */
	String s;

	/**
	 * A reference to the other object (if any).
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	MisuseWithThread item;

	/**
	 * Register a new object in the object `a` and initialize itself afterwards.
	 */
	MisuseWithThread (@org.checkerframework.checker.initialization.qual.UnderInitialization(MisuseWithThread.class) MisuseWithThread referer) throws InterruptedException
	{
		referer.item = this;
		Thread.sleep (1_000);
		s = "";
		item = this;
	}
    
}
