/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class Use {

/* Testing */

	/**
	 * A reference to the other object.
	 */
	var item: Use

	/**
	 * Initialize a new object using other new objects linked in a cycle of 3 objects.
	 */
	constructor ()
	{
		item = Use (this, 2)
	}

	/**
	 * Initialize a new object using other new objects linked in a chain `count` objects terminating with `root`.
	 */
	constructor (root: Use, count: Int)
	{
		if (count > 1) {
			item = Use (root, count - 1)
		}
		else {
			item = root
		}
	}

}

/**
 * Create a cycle of 3 objects.
 */
fun main(args: Array <String>) {
	var a: Use = Use ()
	var b: Use = a.item
	var i: Int = 0
	while (b != a) {
		i = i + 1
		b = b.item
	}
	println (if (i == 2) "OK" else "FAILED");
}
