# Out-of-order transfer of an incompletely initialized object #

## Problematic scenario ##

1. Start a creation of a new object A by calling its constructor.
2. Create an exception object B referencing the current object A before initializing A.
3. Raise an exception with the exception object B.
4. Retrieve the object A from the caught exception object B.
5. Access an uninitialized field of the object A.

## Expected results ##

### Sound result ###

Compile-time error.

### Expectations from theory and documentation ###

Framework              | Compile time | Sound?
-----------------------|:------------:|:------------:
Eiffel                 | Error        | Yes
Java Checker Framework | Error        | Yes
Kotlin                 | No error     | No
