/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class Use {

/* Setup */

	/**
	 * Initialize Current object.
	 */
	constructor ()
	{
		s = ""
	}

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	var s: String

	/**
	 * A reference to the other object (if any).
	 */
	var item: Use? = null

	/**
	 * Initialize a new object and register it in `referer`.
	 */
	constructor (referer: Use)
	{
		s = "OK"
		referer.item = this
	}

}

/**
 * Create a fully initialized object and pass it to the one to be created.
 */
fun main(args: Array <String>) {
	var a: Use = Use ()
	Use (a)
	var b: Use? = a.item
	if (b != null) {
		println (b.s)
	}
	else {
		println ("FAILED")
	}
}
