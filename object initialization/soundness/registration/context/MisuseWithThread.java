/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class MisuseWithThread {

/* Setup */

	/**
	 * Create a fully initialized object and use it in the one to be created.
	 */
	public static void main (String [] args) throws InterruptedException {
			/* Launch a new thread. */
		new Thread (new Runnable () {
			public void run () {
				MisuseWithThread b = null;
				while (b == null) {
					try {
						Thread.sleep (1);
					}
					catch (Exception e) {};
					b = a.item;
				}
				MisuseWithThread x = b.value.value;
			}
		}).start ();
			/* Perform the test. */
		new MisuseWithThread (0);
	}

	/**
	 * Initialize Current object.
	 */
	MisuseWithThread ()
	{
		value = this;
	}

	/**
	 * A completely initialized object.
	 */
	static MisuseWithThread a = new MisuseWithThread ();

/* Testing */

	/**
	 * A value that is set before the object is used.
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	MisuseWithThread value;

	/**
	 * A reference to the other object (if any).
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	@javax.annotation.Nullable
	MisuseWithThread item;

	/**
	 * Register a new object in the object `a` and initialize itself afterwards.
	 */
	MisuseWithThread (int unused) throws InterruptedException
	{
		a.item = this;
		Thread.sleep (1_000);
		value = this;
	}
    
}
