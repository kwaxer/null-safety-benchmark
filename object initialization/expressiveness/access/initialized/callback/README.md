# Calling back to a new object passed from its constructor as an argument to another constructor #

## Scenario ##

1. Create and fully initialize an object A.
2. Pass A as an argument to the constructor of an object B.
3. Make a call on the object A from the constructor of the object B.
4. Initialize B in its constructor as required.
5. Observe changes made at step 3 by the callback.

## Expected results ##

### Sound result ###

No error.

### Expectations from theory and documentation ###

Framework              | Compile time | Run-time output | Expressive?
-----------------------|:------------:|:---------------:|:-----------:
Eiffel                 | No error     | `OK`            | Yes
Java Checker Framework | Error        |                 | No
Kotlin                 | No error     | `OK`            | Yes
