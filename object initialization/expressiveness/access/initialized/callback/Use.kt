/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA 4.0") License.
 */

class Use {

/* Testing */

	/**
	 * A value of the object.
	 */
	var item: String

	/**
	 * Initialize Current object and pass it to a new one for making a callback.
	 */
	constructor ()
	{
		item = "O"
		Use (this)
	}

	/**
	 * Initialize a new object and make a callback on `referer`.
	 */
	constructor (referer: Use)
	{
		referer.append ("K")
		item = "FAILED"
	}

	/**
	 * Append `value` to `item`.
	 */
	fun append (value: String)
	{
		item = item.plus (value)
	}
}

/**
 * Create a fully initialized object and pass it to the one to be created that makes a callback to the first object.
 */
fun main(args: Array <String>) {
	println ((Use ()).item)
}
