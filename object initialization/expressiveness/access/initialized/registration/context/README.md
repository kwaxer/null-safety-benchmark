# Registration of a new object in an existing one retrieved from the execution context #

## Scenario ##

1. Create a fully initialized object A.
2. Make sure the object A is available in the execution context.
3. Initialize B in its constructor as required.
4. Inside the constructor of the object B, register it in the object A so that the object A references the object B.
5. Safely access B via A with regular execution or out-of-order transfer.

## Examples ##

The examples are distinguished by how the step 5 is achieved. The transfer to the point where fields of the object B are accessed can be done by

* regular execution
* an exception
* concurrent execution in another thread

## Expected results ##

### Sound result ###

No error.

### Expectations from theory and documentation ###

Framework              | Compile time | Run-time output | Expressive?
-----------------------|:------------:|:---------------:|:-----------------:
Eiffel                 | No error     | `OK`            | Yes
Java Checker Framework | Error        |                 | No
Kotlin                 | No error     | `OK`            | Yes
