note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class MISUSE_WITH_THREAD

inherit

	THREAD
		rename
			make as thread_make
		end

create
	make,
	test

feature {NONE} -- Setup

	make
			-- Initialize the current object and pass it to create a new one that records itself in the current object.
			-- Access the other object from a different thread before it finishes its initialization.
		do
				-- Initialize Current object.
			s := ""
				-- Launch a new thread.
			thread_make
			launch
				-- Perform the test.
			;(create {MISUSE_WITH_THREAD}.test (Current)).do_nothing
		end

	execute
			-- <Precursor>
		do
				-- Wait until `item` is set.
			from
			until
				attached item as x
			loop
				sleep (1_000_000)
			end
				-- Access the object referenced by `item`.
			io.put_integer (x.s.count)
			io.put_new_line
		end

feature -- Testing

	s: STRING
			-- A value that is set before the object is used.

	item: detachable MISUSE_WITH_THREAD
			-- A reference to the other object (if any).

	put (value: like item)
			-- Set `item` to `value`.
		do
			item := value
		end

	test (referer: MISUSE_WITH_THREAD)
			-- Register a new object in `referer` and initialize it afterwards.
		do
				-- Initialize data of {THREAD}.
			thread_make
				-- Register the current incompletely initialized object in `referer`.
			referer.put (Current)
				-- Let other thread that has a reference to `referer` access the current object.
			sleep (1_000_000_000)
				-- Complete initialization.
			s := ""
		end

end
