/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class Use {

/* Setup */

	/**
	 * Create an object referencing itself.
	 */
	public static void main (String [] args)
	{
			/* Perform the test. */
		Use a = new Use ();
		System.out.println (a == a.item? "OK": "FAILED");
	}

/* Testing */

	/**
	 * A reference to the other object.
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	Use item;

	/**
	 * Initialize a new object with a reference to itself.
	 */
	Use ()
	{
		item = this;
	}

}
