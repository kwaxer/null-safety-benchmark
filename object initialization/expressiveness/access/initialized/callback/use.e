note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class USE

create
	make,
	test,
	test_callback

feature {NONE} -- Setup

	make
			-- Create a fully initialized object and pass it to the one to be created that makes a callback to the first object.
		do
				-- Initialize Current object.
			item := "FAILED"
				-- Perform the test.
			io.put_string ((create {USE}.test).item)
		end

feature -- Testing

	item: STRING
			-- A value of the object.

	append (value: STRING)
			-- Append `value` to `item`.
		do
			item.append (value)
		end

	test
			-- Initialize Current object and pass it to a new one for making a callback.
		do
			item := "O"
			(create {USE}.test_callback (Current)).do_nothing
		end

	test_callback (referer: USE)
			-- Initialize a new object and make a callback on `referer`.
		do
			referer.append ("K")
			item := "FAILED"
		end

end