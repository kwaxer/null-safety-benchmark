/**
 * Copyright (C) 2017 Alexander Kogtenkov
 *
 * This work is licensed under a <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a> License (CC BY-NC-SA 4.0).
 */

class MisuseWithException {

/* Setup */

	/**
	 * Create a new object and catch an exception to retrieve it in an uninitialized state.
	 */
	public static void main (String [] args) {
		try {
			new MisuseWithException ();
		}
		catch (MyException e) {
			String x = e.value.item.s;
			if (x != null) {
				System.out.println (x.length ());
			}
		}
	}

/* Testing */

	/**
	 * A value that should be set before the object is used.
	 */
	String s;

	/**
	 * A value that should be set before the object is used.
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	MisuseWithException item;

	/**
	 * Raise an exception before initializing the new object.
	 */
	MisuseWithException () throws MyException
	{
	    		/* Raise an exception with a reference to the current object. */
		throw new MyException (this);
	}

}

class MyException extends Exception {

	/**
	 * A reference to the object that has to be initialized.
	 */
	@org.checkerframework.checker.initialization.qual.UnknownInitialization
	MisuseWithException value;

	MyException (@org.checkerframework.checker.initialization.qual.UnderInitialization MisuseWithException v) {
		super ();
		value = v;
	}

}
