note
	copyright: "Copyright (C) 2017 Alexander Kogtenkov"
	license: "This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License."
	license_short: "CC BY-NC-SA 4.0"
	license_link: "http://creativecommons.org/licenses/by-nc-sa/4.0/"

class USE

create
	make,
	test,
	test_access

feature {NONE} -- Setup

	make
			-- Create an incompletely initialized object and pass it to the one to be created that makes a callback to the first object.
		do
				-- Initialize Current object.
			item := "FAILED"
			data := "FAILED"
				-- Perform the test.
			io.put_string ((create {USE}.test).item)
		end

feature -- Testing

	item: STRING
			-- A value of the object.

	data: STRING
			-- Some data.

	put (value: STRING)
			-- Set `item` to `value`.
		do
			item := value
		end

	test
			-- Pass Current object to a new one for making a callback and complete initialization afterwards.
		do
			item := "FAILED"
			(create {USE}.test_access (Current)).do_nothing
			data := "FAILED"
		end

	test_access (referer: USE)
			-- Initialize a new object and make a callback on `referer`.
		do
			item := "FAILED"
			data := "FAILED"
			if attached referer.item and then not attached referer.data then
				referer.put ("OK")
			end
		end

end